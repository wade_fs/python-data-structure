#!/usr/bin/python3
print("分『原始』型態與『衍生』型態兩種")
print("原始型態有: 整數(Integer)，浮點數(Float)，字串(String)，布林值(Boolean)")
print("\nPython 的整數包括負無限大到正無限大....口氣很大，跟一般語言不同，它沒有界限")
# Python 在設變數時很隨便，像下面這樣就是宣告並給初值
a=123456789012345678901234567890
b=987654321098765432109876543210
print("a=", a, " +\nb=", b, " =\n ", a+b)
print("a*b = ", a*b)

print("\n浮點數又叫有理數，因為不可能處理『無限位』的無理數, 底下是幾個範例")
x = 4.5
y = 2.4

# 運算
print("底下是一般運算\nx=",x, ", y=", y)
print("x+y=", x + y)
print("x-y=", x - y)
print("x*y=", x * y)
print("x/y=", x / y)
print("餘數: x % y=", x%y) 
print("絕對值 abs(y-x)=", abs(y-x))
print("指數 x^y=",x ** y)
print("\n型態變換:", int(x), int(x*2), int(x+y), int(x+y+0.1))
