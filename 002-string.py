#!/usr/bin/python3
print("\nPython 的字串，一般而言可以『相加』:")
x = 'Cake'
y = 'Cookie'
z = 'hello, wade'
digits = "0123456789"
digitsF = "12345678.9"

print("x + ' & ' + y =", x + ' & ' + y)

print("\n也可以乘以整數:x*2=", x*2, "，而y*3=", y*3)

print("\n字串的切割可以想成是 c 的字元陣列: x[2:] = ", x[2:], " 是「第 2 以後」之意")
print("y[0] + y[3] = ", y[0]+y[3])

print("\n求長度用 len(): len(x+y) = ", len(x+y))
print("首字變大寫:", z.capitalize())
print("每個 Word 開頭變大寫:",z.title())
print("全部變大寫與小寫:",x.upper(), x.lower())

print("\n", digits, " is digits ? ", digits.isdigit())
print("字串的取代:", digits, " => ", digits.replace("0", "零").replace("9", "玖"))
print("字串的取代，在",y,"中找 oo: ", y.find("oo"),"，找 kie:", y.find("ie"))

print("\n還有很多方法，這邊最後講一個格式化常用方法(最後一例含有轉換成整數用法):")
print("像這句裡面有 '{0}', '{2}', '{1}', '{3}', '{4}', '{5}'".format(x, y, z, int(digits)+10, float(digitsF)+0.1, int(float(digitsF))))
