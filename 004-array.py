#!/usr/bin/python3
# print("Python 的陣列有很多種不同用法，包括 list, tuple, set,,,,")
# print("先講最常見的 list, 這個最像 c/c++ 中的陣列, 但是它是動態可變的")
l0 = []
l1 = list()
l2 = list("Hello")
l3 = ['Hello', 'wade']
l0.append("line1")
l1.append(l0)
l1.append(l0)
print("'{0}', '{1}', '{2}', '{3}'".format(l0, l1, l2, l3))
print("長度分別為: '{0}', '{1}', '{2}', '{3}'".format(len(l0), len(l1), len(l2), len(l3)))

print("\n取部份值: '{0}', '{1}', '{2}', '{3}'".format(l1[1:], l2[1:4], l2[:3], l2[:]))
print("也可以『加法』:", l2+l3)
print("進行內插之前:", l2, "檢查 xXX 是否已經在 list 中:", "xXx" in l2)
l2.insert(3, "xXx")
print("進行內插:", l2, "檢查 xXX 是否已經在 list 中:", "xXx" in l2)

# print("\n其他操作: remove()、index()、count()、join()、sort()、copy()、sort()、pop()、reverse()")
print("\n第二種叫 tuple, 它很像 list, 但是是唯讀的，就是不可以變動")
languages = 'tw', 'cn', 'en'
print("languages: ", languages)
#print("tuple 的好處: 1) 佔用空間較少, 2) 可當字典的 key, 3) 具名 tuple 可當做物件, 4) 當做函式引數")

print("\n第三種叫 dict, 別的語言或許叫 hash 或 map, 中文或叫雜湊或叫關聯等")
python = {
  'n' : "python",
  'v' : 3.5
}
print("python:", python)
print("單獨取值: name is ", python['n'], ", version is ", python['v'])
js = {
  'n' : "js",
  "v" : 6
}

import copy
x = python
y = dict(js)
print("接下來看看複製: x=python 之後，修改 x.v: ", python, x)
print("\t\ty=dict(js)之後，修改 y.v: ", js, y)
# print("你會發現，x 跟 python 是同一個物件，類似 c 中的指標，並沒有真的複製")

y["o"] = dict(python)
z = copy.deepcopy(y)
zz = dict(y)
print("\nz=copy.deepcopy(y), zz=dict(y):\n\ty ={0}\n\tz ={1}\n\tzz={2}".format(y, z, zz))
z['o']['v'] = 12.0
zz['o']['v'] = 13.0
print("修改 : \n\ty ={0}\n\tz ={1}\n\tzz={2}".format(y, z, zz))
print("z 因為是 deepcopy(), 所以z['o'] 是複製品，而zz['o'] 則是指標(同y)")
#print("有沒有注意到 list : [], tuple : (), dict : {} ?")
# 在寫程式時，還會有 js.keys(), js.values(), js.items() 這些方法可用
# 同樣也可以用 'name' in js 這種判斷法

print("\n第四種是 set, l2={0}, languages={1}".format(l2, languages))
print("union: ", set(l2).union(set(languages)))
print("intersection: ", set(l2).intersection(set(languages)))
print("difference: ", set(l2).difference(languages))
# set 無次序性，不會重複

import array as arr
#print("Python 中 array 跟 list 看起來很像，但是實際上差別很大")
#print("首先就是 array 需要 import, 再來就是語法不同")
ll = list ("Hello")
aa = arr.array("u",["c","a","t","s"]) # "u" 是指 unsigned char, 相當於字串
print("\n第五種是陣列:\nll={0}, aa={1}".format(ll, aa))

# 再來就是，用最多 array 的是 numpy 模組，我們不會特別講 numpy
import numpy as np
l0 = [3,6,9]
npa = np.array(l0)
print("l0={0}, npa={1}, npa/3={2}".format(l0, npa, npa/3))

