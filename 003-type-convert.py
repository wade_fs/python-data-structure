#!/usr/bin/python3
s = "4.5"
t = "2.4"
x = 4.5
y = 2.4

# print("如果直接數字+字串會出錯", s+x)
print("基礎型態變換:", int(x), int(x*2), int(x+y), int(x+y+0.1))
print("{0} + {1} = {2}".format(x, y, x+y))
print("變成字串的話: {0} + {1} = {2}".format(x, y, str(x)+str(y)))

print("\n字串:'{0}', '{1}', '{2}'".format(s, t, s+t))
print("\n字串變實數:'{0}', '{1}', '{2}'".format(s, t, float(s)+float(t)))
print("\n字串變整數:'{0}', '{1}', '{2}', '{3}'".format(s, t, int(float(s))+int(float(t))+0.1, int(float(s)+float(t)+0.1)))
